var calculatorViewModel = function() {
    var self = this;
    self.displayValue = ko.observable('');
    self.lastCommand = ko.observable('');
    self.needCleanup = ko.observable(false);
    self.periodCount = 0;

    self.addNumber = function(val) {
        if (self.needCleanup()) {
            self.displayValue('');
            self.needCleanup(false);
        }

        if (val == 0 && self.displayValue() == '') {
            return;
        }

        if(val == '.'){
            self.periodCount = self.periodCount + 1;
            if(self.periodCount > 1) {
                return;
            }
        }

        self.displayValue(self.displayValue() + val);
    };

    self.addCommand = function(command) {
        if (self.lastCommand() == '') {
            self.displayValue(self.displayValue() + command);
            self.lastCommand(command);
        }
    };

    self.doCalculate = function() {
        self.displayValue(eval(self.displayValue()));
        if (self.lastCommand() != '') {
            self.lastCommand('');
        }
        self.needCleanup(true);
    };

    self.clearValues = function() {
        self.displayValue('');
        self.needCleanup(false);
        self.lastCommand('');
    }

    self.hasNumbers = ko.computed(function() {
        return self.displayValue() == '';
    }, self);
}

ko.applyBindings(new calculatorViewModel());