function shoppingList(productId,name,type,quantity,availability,discountType)
{
    var self = this;
    self.productId = ko.observable(productId);
    self.name = ko.observable(name);
    self.type = ko.observable(type);
    self.quantity = ko.observable(quantity);
    self.availability = ko.observable(availability);
    self.discountType = ko.observableArray(discountType);
}

var orderFormViewModel = function(){
    var self = this;

    self.availableCategories = ["mobile", "pc"];
    self.shouldShowOrders = ko.observable(false);

    self.shoppingOrders = ko.observableArray([
        new shoppingList('123','sample product', self.availableCategories[0],'2', '' , [])
    ]);

    self.addRow = function() {
        self.shoppingOrders.push(
            new shoppingList('','', self.availableCategories[0],'', '' , [])
        )
    }

    self.removeRow = function(row) {
        self.shoppingOrders.remove(row)
    }

    self.showOrders = function() {
        self.shouldShowOrders(true);
    }

    self.resetOrders = function() {
        self.shoppingOrders.splice(1);
        self.shouldShowOrders(false);
    }

}

ko.applyBindings(new orderFormViewModel());