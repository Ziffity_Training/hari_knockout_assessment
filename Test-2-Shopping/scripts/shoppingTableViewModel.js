function ProductModel(data) {
    if(!data) {
        data = {}
    }
    
    var self = this;
    self.id = data.id;
    self.name = data.name;
    self.sku = data.sku;
    self.price = data.price;
    self.description = data.description;
}

function ShoppingPaginationModel (data) {
    if(!data) {
        data = {}
    }

    var self = this;
    self.products = ExtractModels(self, data.products, ProductModel);

    var filters = [
        {
            Type: "price",
            Name: "Price",
            Options: [
                GetOption("Choose Filter", "All", null),
                GetOption("0.00$-0.25$","0.00-0.25","0.00-0.25"),
                GetOption("0.25$-0.50$","0.25-0.50","0.25-0.50"),
                GetOption("0.50$-0.75$","0.50-0.75","0.50-0.75")
            ],
            CurrentOption: ko.observable(),
            RecordValue: function(record) { return record.price; }
        }
    ];
    var sortOptions = [
        {
            Name: "Id",
            Value: "Id",
            Sort: function(left, right) { return left.id < right.id }
        },
        {
            Name: "Price",
            Value: "Price",
            Sort: function(left, right) { return left.price < right.price }
        }
    ];
    self.filter = new FilterModel(filters, self.products);
    self.sorter = new SorterModel(sortOptions, self.filter.filteredRecords);
    self.pager = new PaginationModel(self.sorter.orderedRecords);

    self.pages = ['plp', 'pdp'];
    self.chosenProductData = ko.observable();
    self.showPlp = ko.observable(true);

    self.goToPage = function(page) { 
        location.hash = page;
    };
    self.goToProduct = function(product) { 
        location.hash = 'pdp/' + product.id;
        self.chosenProductData(product);
    };

    Sammy(function() {
        this.get('#:page', function() {
            self.showPlp(true);
        });

        this.get('#:page/:listing', function() {
            self.showPlp(false);
        });

        this.get('', function() {this.app.runRoute('get','#plp')});
    }).run();
}

function PaginationModel(records) {
    var self = this;
	
	self.records = GetObservableArray(records);
	self.currentPageIndex = ko.observable(self.records().length > 0 ? 0 : -1);
	self.currentPageSize = ko.observable(10);
	self.recordCount = ko.computed(function() {
		return self.records().length;
	});
	self.maxPageIndex = ko.computed(function() {
		return Math.ceil(self.records().length / self.currentPageSize()) - 1;
	});
	self.currentPageRecords = ko.computed(function() {
		var newPageIndex = -1;
		var pageIndex = self.currentPageIndex();
		var maxPageIndex = self.maxPageIndex();
		if (pageIndex > maxPageIndex)
		{
			newPageIndex = maxPageIndex;
		}
		else if (pageIndex == -1)
		{
			if (maxPageIndex > -1)
			{
				newPageIndex = 0;
			}
			else
			{
				newPageIndex = -2;
			}
		}
		else
		{
			newPageIndex = pageIndex;
		}
		
		if (newPageIndex != pageIndex)
		{
			if (newPageIndex >= -1)
			{
				self.currentPageIndex(newPageIndex);
			}

			return [];
		}

		var pageSize = self.currentPageSize();
		var startIndex = pageIndex * pageSize;
		var endIndex = startIndex + pageSize;
		return self.records().slice(startIndex, endIndex);
	}).extend({ throttle: 5 });
    
    self.selectPage = function(index) {
        self.changePageIndex(index);
    }

	self.changePageIndex = function(newIndex) {
		if (newIndex < 0
			|| newIndex == self.currentPageIndex()
			|| newIndex > self.maxPageIndex())
		{
			return;
		}

		self.currentPageIndex(newIndex);
	};
	self.onPageSizeChange = function() {
		self.currentPageIndex(0);
	};

	self.renderNoRecords = function() {
		var message = "<span data-bind=\"visible: pager.recordCount() == 0\">No records found.</span>";
		$("div.NoRecords").html(message);
	};

	self.renderNoRecords();
}

function SorterModel(sortOptions, records) {
    var self = this;
    self.records = GetObservableArray(records);
    self.sortOptions = ko.observableArray(sortOptions);
    self.sortDirections = ko.observableArray([
        {
            Name: "Select Sort",
            Value: "Asc",
            Sort: false
        },
        {
            Name: "Asc",
            Value: "Asc",
            Sort: false
        },
        {
            Name: "Desc",
            Value: "Desc",
            Sort: true
        }
    ]);

    self.currentSortOption = ko.observable(self.sortOptions()[0]);
    self.currentSortDirection = ko.observable(self.sortDirections()[0]);
    self.orderedRecords = ko.computed(function () {
        var records = self.records();
        var sortOption = self.currentSortOption();
        var sortDirection = self.currentSortDirection();
        if(sortOption == null || sortDirection == null) {
            return records;
        }
        var sortedRecords = records.slice(0,records.length);
        SortArray(sortedRecords, sortDirection.Sort, sortOption.Sort);
        return sortedRecords;
    }).extend({ throttle: 5 });
}

function FilterModel(filters, records) {
    var self = this;
    self.records = GetObservableArray(records);
    self.filters = ko.observableArray(filters);
    self.activeFilters = ko.computed(function () {
        var filters = self.filters();
        var activeFilters = [];
        
        for(var i=0; i<filters.length; i++) {
            var filter = filters[i];

            if(filter.CurrentOption) {
                var filterOption = filter.CurrentOption();
                if (filterOption && filterOption.FilterValue != null) {
                  var activeFilter = {
                    Filter: filter,
                    IsFiltered: function (filter, record) {
                      var filterOption = filter.CurrentOption();
                      if(!filterOption) {
                        return;
                      }
        
                      var recordValue = filter.RecordValue(record);
                      recordValue = Math.trunc(recordValue * 100);
                      var filterRange = filterOption.FilterValue.split("-");
                      var filterRangeMax = Math.trunc(filterRange[1] * 100);
                      var filterRangeMin = Math.trunc(filterRange[0] * 100);
                      return !(recordValue <= filterRangeMax && recordValue >= filterRangeMin); 
                    }
                  };
                  activeFilters.push(activeFilter);
                }
              }
        }

        return activeFilters;
    });
    self.filteredRecords = ko.computed(function () {
        var records = self.records();
        var filters = self.activeFilters();
        if(filters.length == 0) {
            return records;
        }

        var filteredRecords = [];
        for( var rIndex = 0; rIndex < records.length; rIndex++) {
            var isIncluded = true;
            var record = records[rIndex];
            for(var fIndex = 0; fIndex < filters.length; fIndex++) {
                var filter = filters[fIndex];
                var IsFiltered = filter.IsFiltered(filter.Filter,record);
                if(IsFiltered) {
                    isIncluded = false;
                    break;
                }
            }

            if(isIncluded) {
                filteredRecords.push(record);
            }
        }
        return filteredRecords;
    }).extend({ throttle: 200 });
}

function ExtractModels(parent, data, constructor) 
{
  var models = [];
  if(data == null) {
    return models;
  }

  for(var i=0; i<data.length; i++) {
    var row = data[i];
    var model = new constructor(row, parent);
    models.push(model);
  }
  
  return models;
}

function GetObservableArray(array)
{
    if (typeof(array) == 'function')
	{
		return array;
    }

	return ko.observableArray(array);
}

function GetOption(name, value, filterValue) {
    var option = {
        Name: name,
        Value: value,
        FilterValue: filterValue
    };
    
    return option;
}

function SortArray(array, direction, comparison) {
    if(array == null) {
        return [];
    }

    for(var oIndex = 0; oIndex < array.length; oIndex++) {
        var oItem = array[oIndex];
        for(var iIndex = oIndex + 1; iIndex < array.length; iIndex++) {
            var iItem = array[iIndex];
            var isOrdered = comparison(oItem, iItem);
            if(isOrdered == direction) {
                array[iIndex] = oItem;
                array[oIndex] = iItem;
                oItem = iItem;
            }
        }
    }

    return array;
}

var testProductData = [{ "id": 1, "name": "Product 1", "sku": "2781", "price": 0.45, "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 2, "name": "Product 2", "sku": "1900", "price": 0.36, "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.", "image": "wire.jpeg" },
{ "id": 3, "name": "Product 3", "sku": "6109", "price": 0.87, "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.", "image": "wire.jpeg" },
{ "id": 4, "name": "Product 4", "sku": "6033", "price": 0.5, "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.", "image": "wire.jpeg" },
{ "id": 5, "name": "Product 5", "sku": "6236", "price": 0.98, "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.", "image": "wire.jpeg" },
{ "id": 6, "name": "Product 6", "sku": "5604", "price": 0.38, "description": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\n\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 7, "name": "Product 7", "sku": "6559", "price": 0.8, "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.", "image": "wire.jpeg" },
{ "id": 8, "name": "Product 8", "sku": "8765", "price": 0.68, "description": "Sed ante. Vivamus tortor. Duis mattis egestas metus.", "image": "wire.jpeg" },
{ "id": 9, "name": "Product 9", "sku": "1589", "price": 0.69, "description": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.", "image": "wire.jpeg" },
{ "id": 10, "name": "Product 10", "sku": "3167", "price": 0.4, "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.", "image": "wire.jpeg" },
{ "id": 11, "name": "Product 11", "sku": "0384", "price": 0.26, "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.", "image": "wire.jpeg" },
{ "id": 12, "name": "Product 12", "sku": "4895", "price": 0.96, "description": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.", "image": "wire.jpeg" },
{ "id": 13, "name": "Product 13", "sku": "0153", "price": 0.9, "description": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.", "image": "wire.jpeg" },
{ "id": 14, "name": "Product 14", "sku": "6192", "price": 0.48, "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.", "image": "wire.jpeg" },
{ "id": 15, "name": "Product 15", "sku": "5127", "price": 0.46, "description": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\n\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.", "image": "wire.jpeg" },
{ "id": 16, "name": "Product 16", "sku": "7959", "price": 0.42, "description": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\n\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 17, "name": "Product 17", "sku": "8714", "price": 0.97, "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.", "image": "wire.jpeg" },
{ "id": 18, "name": "Product 18", "sku": "2844", "price": 0.28, "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.", "image": "wire.jpeg" },
{ "id": 19, "name": "Product 19", "sku": "0819", "price": 0.88, "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.", "image": "wire.jpeg" },
{ "id": 20, "name": "Product 20", "sku": "2827", "price": 0.04, "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 21, "name": "Product 21", "sku": "4544", "price": 0.7, "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.", "image": "wire.jpeg" },
{ "id": 22, "name": "Product 22", "sku": "4796", "price": 0.27, "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.", "image": "wire.jpeg" },
{ "id": 23, "name": "Product 23", "sku": "6281", "price": 0.86, "description": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.", "image": "wire.jpeg" },
{ "id": 24, "name": "Product 24", "sku": "3038", "price": 0.12, "description": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.", "image": "wire.jpeg" },
{ "id": 25, "name": "Product 25", "sku": "5565", "price": 0.18, "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 26, "name": "Product 26", "sku": "9511", "price": 0.26, "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", "image": "wire.jpeg" },
{ "id": 27, "name": "Product 27", "sku": "9519", "price": 0.09, "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.", "image": "wire.jpeg" },
{ "id": 28, "name": "Product 28", "sku": "4663", "price": 0.08, "description": "Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.", "image": "wire.jpeg" },
{ "id": 29, "name": "Product 29", "sku": "0452", "price": 0.08, "description": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\n\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.", "image": "wire.jpeg" },
{ "id": 30, "name": "Product 30", "sku": "8448", "price": 0.96, "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.", "image": "wire.jpeg" },
{ "id": 31, "name": "Product 31", "sku": "0397", "price": 0.98, "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.", "image": "wire.jpeg" },
{ "id": 32, "name": "Product 32", "sku": "0562", "price": 0.32, "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 33, "name": "Product 33", "sku": "8824", "price": 0.31, "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 34, "name": "Product 34", "sku": "6215", "price": 0.19, "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", "image": "wire.jpeg" },
{ "id": 35, "name": "Product 35", "sku": "0333", "price": 0.98, "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.", "image": "wire.jpeg" },
{ "id": 36, "name": "Product 36", "sku": "5770", "price": 0.68, "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.", "image": "wire.jpeg" },
{ "id": 37, "name": "Product 37", "sku": "9680", "price": 0.16, "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.", "image": "wire.jpeg" },
{ "id": 38, "name": "Product 38", "sku": "5780", "price": 0.72, "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.", "image": "wire.jpeg" },
{ "id": 39, "name": "Product 39", "sku": "2701", "price": 0.43, "description": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\n\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.", "image": "wire.jpeg" },
{ "id": 40, "name": "Product 40", "sku": "6715", "price": 0.03, "description": "Sed ante. Vivamus tortor. Duis mattis egestas metus.", "image": "wire.jpeg" },
{ "id": 41, "name": "Product 41", "sku": "1585", "price": 0.04, "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", "image": "wire.jpeg" },
{ "id": 42, "name": "Product 42", "sku": "5821", "price": 0.18, "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.", "image": "wire.jpeg" },
{ "id": 43, "name": "Product 43", "sku": "5370", "price": 0.33, "description": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.", "image": "wire.jpeg" },
{ "id": 44, "name": "Product 44", "sku": "7879", "price": 0.21, "description": "Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.", "image": "wire.jpeg" },
{ "id": 45, "name": "Product 45", "sku": "6827", "price": 0.75, "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.", "image": "wire.jpeg" },
{ "id": 46, "name": "Product 46", "sku": "3817", "price": 0.35, "description": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.", "image": "wire.jpeg" },
{ "id": 47, "name": "Product 47", "sku": "9562", "price": 0.52, "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.", "image": "wire.jpeg" },
{ "id": 48, "name": "Product 48", "sku": "8078", "price": 0.07, "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.", "image": "wire.jpeg" },
{ "id": 49, "name": "Product 49", "sku": "3567", "price": 0.12, "description": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.", "image": "wire.jpeg" },
{ "id": 50, "name": "Product 50", "sku": "0253", "price": 0.03, "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.", "image": "wire.jpeg" },
{ "id": 51, "name": "Product 51", "sku": "2029", "price": 0.55, "description": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.", "image": "wire.jpeg" },
{ "id": 52, "name": "Product 52", "sku": "1267", "price": 0.93, "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.", "image": "wire.jpeg" },
{ "id": 53, "name": "Product 53", "sku": "5159", "price": 0.2, "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.", "image": "wire.jpeg" },
{ "id": 54, "name": "Product 54", "sku": "3191", "price": 0.87, "description": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.", "image": "wire.jpeg" },
{ "id": 55, "name": "Product 55", "sku": "8463", "price": 0.63, "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.", "image": "wire.jpeg" },
{ "id": 56, "name": "Product 56", "sku": "7236", "price": 0.71, "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.", "image": "wire.jpeg" },
{ "id": 57, "name": "Product 57", "sku": "5901", "price": 0.86, "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.", "image": "wire.jpeg" },
{ "id": 58, "name": "Product 58", "sku": "5516", "price": 0.23, "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.", "image": "wire.jpeg" },
{ "id": 59, "name": "Product 59", "sku": "9314", "price": 0.11, "description": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.", "image": "wire.jpeg" },
{ "id": 60, "name": "Product 60", "sku": "6264", "price": 0.78, "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.", "image": "wire.jpeg" },
{ "id": 61, "name": "Product 61", "sku": "2301", "price": 0.28, "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. MaeShoppingcenas pulvinar lobortis est.", "image": "wire.jpeg" },
{ "id": 62, "name": "Product 62", "sku": "4630", "price": 0.62, "description": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.", "image": "wire.jpeg" },
{ "id": 63, "name": "Product 63", "sku": "8945", "price": 0.75, "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.", "image": "wire.jpeg" },
{ "id": 64, "name": "Product 64", "sku": "2262", "price": 0.85, "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.", "image": "wire.jpeg" },
{ "id": 65, "name": "Product 65", "sku": "2604", "price": 0.96, "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.", "image": "wire.jpeg" },
{ "id": 66, "name": "Product 66", "sku": "0162", "price": 0.97, "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.", "image": "wire.jpeg" },
{ "id": 67, "name": "Product 67", "sku": "1312", "price": 0.69, "description": "Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.", "image": "wire.jpeg" },
{ "id": 68, "name": "Product 68", "sku": "5302", "price": 0.19, "description": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.", "image": "wire.jpeg" },
{ "id": 69, "name": "Product 69", "sku": "6878", "price": 0.87, "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.", "image": "wire.jpeg" },
{ "id": 70, "name": "Product 70", "sku": "6113", "price": 0.1, "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.", "image": "wire.jpeg" },
{ "id": 71, "name": "Product 71", "sku": "5956", "price": 0.42, "description": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.", "image": "wire.jpeg" },
{ "id": 72, "name": "Product 72", "sku": "5786", "price": 0.2, "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.", "image": "wire.jpeg" },
{ "id": 73, "name": "Product 73", "sku": "3234", "price": 0.85, "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.", "image": "wire.jpeg" },
{ "id": 74, "name": "Product 74", "sku": "8760", "price": 0.96, "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 75, "name": "Product 75", "sku": "6659", "price": 0.58, "description": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.", "image": "wire.jpeg" },
{ "id": 76, "name": "Product 76", "sku": "2272", "price": 0.5, "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.", "image": "wire.jpeg" },
{ "id": 77, "name": "Product 77", "sku": "2101", "price": 0.43, "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.", "image": "wire.jpeg" },
{ "id": 78, "name": "Product 78", "sku": "7827", "price": 0.06, "description": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.", "image": "wire.jpeg" },
{ "id": 79, "name": "Product 79", "sku": "8941", "price": 0.11, "description": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.", "image": "wire.jpeg" },
{ "id": 80, "name": "Product 80", "sku": "2795", "price": 0.64, "description": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.", "image": "wire.jpeg" },
{ "id": 81, "name": "Product 81", "sku": "3901", "price": 0.87, "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.", "image": "wire.jpeg" },
{ "id": 82, "name": "Product 82", "sku": "6031", "price": 0.69, "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.", "image": "wire.jpeg" },
{ "id": 83, "name": "Product 83", "sku": "2706", "price": 0.13, "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.", "image": "wire.jpeg" },
{ "id": 84, "name": "Product 84", "sku": "5034", "price": 0.91, "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.", "image": "wire.jpeg" },
{ "id": 85, "name": "Product 85", "sku": "5228", "price": 0.62, "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 86, "name": "Product 86", "sku": "8875", "price": 0.79, "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.", "image": "wire.jpeg" },
{ "id": 87, "name": "Product 87", "sku": "9971", "price": 0.51, "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.", "image": "wire.jpeg" },
{ "id": 88, "name": "Product 88", "sku": "5449", "price": 0.47, "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "image": "wire.jpeg" },
{ "id": 89, "name": "Product 89", "sku": "5339", "price": 0.95, "description": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.", "image": "wire.jpeg" },
{ "id": 90, "name": "Product 90", "sku": "8522", "price": 0.18, "description": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.", "image": "wire.jpeg" },
{ "id": 91, "name": "Product 91", "sku": "9280", "price": 0.82, "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 92, "name": "Product 92", "sku": "6062", "price": 0.87, "description": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.", "image": "wire.jpeg" },
{ "id": 93, "name": "Product 93", "sku": "5117", "price": 0.9, "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.", "image": "wire.jpeg" },
{ "id": 94, "name": "Product 94", "sku": "6964", "price": 0.23, "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "image": "wire.jpeg" },
{ "id": 95, "name": "Product 95", "sku": "0771", "price": 0.99, "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.", "image": "wire.jpeg" },
{ "id": 96, "name": "Product 96", "sku": "8940", "price": 0.31, "description": "Phasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.", "image": "wire.jpeg" },
{ "id": 97, "name": "Product 97", "sku": "4286", "price": 0.92, "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.", "image": "wire.jpeg" },
{ "id": 98, "name": "Product 98", "sku": "7067", "price": 0.15, "description": "Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.", "image": "wire.jpeg" },
{ "id": 99, "name": "Product 99", "sku": "2057", "price": 0.19, "description": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.", "image": "wire.jpeg" },
{ "id": 100, "name": "Product 100", "sku": "9721", "price": 0.12, "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.", "image": "wire.jpeg" }];

var productData = {
    products: testProductData
}

ko.applyBindings(new ShoppingPaginationModel(productData));
