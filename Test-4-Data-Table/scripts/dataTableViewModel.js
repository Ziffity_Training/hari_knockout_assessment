function UserModel(data) {
  if(!data) {
    data = {}
  }

  var self = this;
  self.id = data.id;
  self.name = data.name;
  self.age = data.age;
  self.office = data.office;
  self.joined_date = data.joined_date;
  self.salary = data.salary;
  self.email = data.email;
  self.phone_number = data.phone_number;
  self.city = data.city;
  self.country = data.country;
  self.zip_code = data.zip_code;
}

function UserPaginationModel(data) {
  if(!data) {
    data = {}
  }

  var self = this;
  self.users = ExtractModels(self, data.users, UserModel);

  var filters = [
    {
      Type: "country",
      Name: "Country",
      Choices: ['India', 'United States', 'Russia'],
      SelectedChoices: ko.observableArray(),
      RecordValue: function(record) { return record.country; }
    },
    {
      Type: "city",
      Name: "City",
      Choices: ['Chennai', 'Delhi', 'Mumbai', 'Kolkata', 'Sacramento', 'Dallas', 'Moscow', 'Stalingrad'],
      SelectedChoices: ko.observableArray(),
      RecordValue: function(record) { return record.city; }
    },
    {
      Type: "age",
      Name: "Age",
      Options: [
        GetOption("choose age", "All", null),
        GetOption("20-30","20-30","20-30"),
        GetOption("31-40","31-40","31-40"),
        GetOption("41-50","41-50","41-50")
      ],
      CurrentOption: ko.observable(),
      RecordValue: function(record) { return record.age; }
    },
    {
      Type: "salary",
      Name: "Salary",
      Options: [
        GetOption("choose salary", "All", null),
        GetOption("21000-31000","21000-31000","21000-31000"),
        GetOption("31001-41000","31001-41000","31001-41000"),
        GetOption("41001-50000","41001-50000","41001-50000")
      ],
      CurrentOption: ko.observable(),
      RecordValue: function(record) { return record.salary; }
    },
    {
      Type: 'reset'
    },
    {
      Type: "bottomhr"
    },
    {
      Type: "search",
      Name: "Search",
      Value: ko.observable(""),
      RecordValue: function(record) { return record.name; }
    }
  ];

  self.filter = new FilterModel(filters, self.users);
  self.pager = new PaginationModel(self.filter.filteredRecords);

  self.resetFilters = function() {
    location.reload();
  }

}

function PaginationModel(records) {
  var self = this;
  self.pageSizeOptions = ko.observableArray([10,25,50,100]);

  self.records = GetObservableArray(records);
  self.currentPageIndex = ko.observable(self.records().length > 0 ? 0 : -1);  //pager.currentPageIndex() + 1 // current page text
  self.currentPageSize = ko.observable(25); //pager.currentPageSize //total no. of records to show
  self.recordCount = ko.computed(function () {  //total no. of records
    return self.records().length;
  });
  self.maxPageIndex = ko.computed(function() { //pager.maxPageIndex() + 1 //max page count - text
    return Math.ceil(self.records().length / self.currentPageSize()) - 1;
  });
  self.currentPageRecords = ko.computed(function() {
    var newPageIndex = -1;
    var pageIndex = self.currentPageIndex();
    var maxPageIndex = self.maxPageIndex();
    if(pageIndex > maxPageIndex) {
      newPageIndex = maxPageIndex;
    } else if(pageIndex == -1) {
      if(maxPageIndex > -1) {
        newPageIndex = 0;
      } else {
        newPageIndex = -2;
      }
    } else {
      newPageIndex = pageIndex;
    }

    if(newPageIndex != pageIndex) {
      if(newPageIndex >= -1) {
        self.currentPageIndex(newPageIndex);
      }
      
      return [];
    }

    var pageSize = self.currentPageSize();
    var startIndex = pageIndex * pageSize;
    var endIndex = startIndex + pageSize;
    return self.records().slice(startIndex, endIndex);
  }).extend({throttle: 5});

  self.selectPage = function(index) {
    self.changePageIndex(index);
  }

  self.changePageIndex = function(newIndex) {
    if(newIndex < 0 || newIndex == self.currentPageIndex() || newIndex > self.maxPageIndex()) {
      return;
    }
    
    self.currentPageIndex(newIndex);
  };

  self.onPageSizeChange = function() {
    self.currentPageIndex(0);
  };

	self.renderNoRecords = function() {
		var message = "<span data-bind=\"visible: pager.recordCount() == 0\">No records found.</span>";
		$("div.NoRecords").html(message);
	};

	self.renderNoRecords();

}

function FilterModel(filters, records)
{
  var self = this;
  self.records = GetObservableArray(records);
  self.filters = ko.observableArray(filters);
  self.activeFilters = ko.computed(function() {
    var filters = self.filters();
    var activeFilters = [];
    for (var i = 0; i < filters.length ; i++) {
      var filter = filters[i];

      if(filter.SelectedChoices) {
        var filterChoices = filter.SelectedChoices();
        if(filterChoices && filterChoices.length > 0) {
          var activeFilter = {
            Filter: filter,
            IsFiltered: function(filter, record) {
              var filterChoices = filter.SelectedChoices();
              if(filterChoices.length < 1) {
                return;
              }
              var recordValue = filter.RecordValue(record);
              return filterChoices.indexOf(recordValue) == -1;
            }
          };
          activeFilters.push(activeFilter);
        }
      }else if(filter.CurrentOption) {
        var filterOption = filter.CurrentOption();
        if (filterOption && filterOption.FilterValue != null) {
          var activeFilter = {
            Filter: filter,
            IsFiltered: function (filter, record) {
              var filterOption = filter.CurrentOption();
              if(!filterOption) {
                return;
              }

              var recordValue = filter.RecordValue(record);
              var filterRange = filterOption.FilterValue.split("-");
              return !(recordValue <= filterRange[1] && recordValue >= filterRange[0]); 
            }
          };
          activeFilters.push(activeFilter);
        }
      }
      else if (filter.Value) {
        var filterValue = filter.Value();
        if (filterValue && filterValue != "") {
          var activeFilter = {
            Filter: filter,
            IsFiltered: function(filter, record) {
              var filterValue = filter.Value();
              filterValue = filterValue.toUpperCase();

              var recordValue = filter.RecordValue(record);
              recordValue = recordValue.toUpperCase();
              return recordValue.indexOf(filterValue) == -1;
            }
          };
          activeFilters.push(activeFilter);
        }
      }
    }

    return activeFilters;
  });
  self.filteredRecords = ko.computed(function() {
    var records = self.records();
    var filters = self.activeFilters();
    if(filters.length == 0) {
      return records;
    }

    var filteredRecords = [];
    for( var rIndex = 0; rIndex < records.length; rIndex++) {
      var isIncluded = true;
      var record = records[rIndex];
      for (var fIndex = 0; fIndex < filters.length; fIndex++) {
        var filter = filters[fIndex];
        var IsFiltered = filter.IsFiltered(filter.Filter, record);
        if(IsFiltered) {
          isIncluded = false;
          break;
        }
      }

      if(isIncluded) {
        filteredRecords.push(record);
      }
    }

    return filteredRecords;
  }).extend({ throttle: 200 });
}

function ExtractModels(parent, data, constructor) 
{
  var models = [];
  if(data == null) {
    return models;
  }

  for(var i=0; i<data.length; i++) {
    var row = data[i];
    var model = new constructor(row, parent);
    models.push(model);
  }
  
  return models;
}

function GetObservableArray(array) {
  if (typeof(array) == 'function') {
    return array;
  }

  return ko.observableArray(array);
}

function GetOption(name, value, filterValue) {
  var option = {
    Name: name,
    Value: value,
    FilterValue: filterValue
  };
  return option;
}

var testUserData = [{"id":1,"name":"Missy Reveland","age":33,"office":"Brekke and Sons","joined_date":"05/01/2019","salary":40236,"email":"mreveland0@jalbum.net","phone_number":"1555130243","city":"Katrineholm","country":"Sweden","zip_code":"641 47"},
{"id":2,"name":"Devon Whittuck","age":35,"office":"Barrows and Sons","joined_date":"29/02/2020","salary":23426,"email":"dwhittuck1@marriott.com","phone_number":"2955086502","city":"Palhais","country":"Portugal","zip_code":"4620-023"},
{"id":3,"name":"Dionis Lebond","age":32,"office":"Robel-Strosin","joined_date":"16/05/2019","salary":37755,"email":"dlebond2@mtv.com","phone_number":"1619072807","city":"Delhi","country":"India","zip_code":null},
{"id":4,"name":"Roselia Milne","age":37,"office":"Howe-Olson","joined_date":"19/08/2019","salary":29849,"email":"rmilne3@earthlink.net","phone_number":"8103834833","city":"Delhi","country":"India","zip_code":null},
{"id":5,"name":"Diane-marie Gebbie","age":36,"office":"Ward-Haag","joined_date":"07/10/2019","salary":36305,"email":"dgebbie4@latimes.com","phone_number":"5822152250","city":"Delhi","country":"India","zip_code":null},
{"id":6,"name":"Bev Pearde","age":43,"office":"Marks Group","joined_date":"19/10/2019","salary":37146,"email":"bpearde5@fastcompany.com","phone_number":"2231655576","city":"Bužim","country":"Bosnia and Herzegovina","zip_code":null},
{"id":7,"name":"Penelope Schrir","age":40,"office":"Abernathy-Wolff","joined_date":"10/10/2020","salary":26436,"email":"pschrir6@51.la","phone_number":"5054955143","city":"Sacramento","country":"United States","zip_code":"87105"},
{"id":8,"name":"Raquel Abells","age":33,"office":"Kovacek, Stark and Douglas","joined_date":"28/04/2020","salary":36504,"email":"rabells7@webmd.com","phone_number":"5921961376","city":"Zhatay","country":"Russia","zip_code":"182514"},
{"id":9,"name":"Charis Kures","age":34,"office":"Bosco-Bradtke","joined_date":"08/09/2019","salary":44725,"email":"ckures8@google.co.jp","phone_number":"5323493363","city":"San Pedro","country":"Philippines","zip_code":"4023"},
{"id":10,"name":"Cassandry Gossling","age":32,"office":"Kemmer Inc","joined_date":"15/09/2019","salary":44440,"email":"cgossling9@twitpic.com","phone_number":"2297365914","city":"Grebenskaya","country":"Russia","zip_code":"366101"},
{"id":11,"name":"Rori Kayzer","age":35,"office":"Herman, Gusikowski and Waters","joined_date":"19/06/2019","salary":39336,"email":"rkayzera@feedburner.com","phone_number":"7631800908","city":"Delhi","country":"India","zip_code":null},
{"id":12,"name":"Loralyn Esposita","age":40,"office":"Leannon-Dibbert","joined_date":"01/06/2020","salary":36697,"email":"lespositab@discovery.com","phone_number":"2524038114","city":"Evijärvi","country":"Finland","zip_code":"62501"},
{"id":13,"name":"West Jakubovitch","age":29,"office":"Witting-Torp","joined_date":"28/11/2019","salary":39956,"email":"wjakubovitchc@t-online.de","phone_number":"6812422752","city":"Vitry-le-François","country":"France","zip_code":"51361 CEDEX"},
{"id":14,"name":"Gretel Rustman","age":28,"office":"Jakubowski and Sons","joined_date":"25/09/2019","salary":25897,"email":"grustmand@google.com.au","phone_number":"2274958241","city":"Laveiras","country":"Portugal","zip_code":"2760-012"},
{"id":15,"name":"Ailee Pinyon","age":23,"office":"Tromp LLC","joined_date":"19/11/2020","salary":29720,"email":"apinyone@apple.com","phone_number":"1455611508","city":"San Ramon","country":"Philippines","zip_code":"4425"},
{"id":16,"name":"Camel Lakenton","age":29,"office":"Homenick, Osinski and Roberts","joined_date":"07/05/2020","salary":47235,"email":"clakentonf@last.fm","phone_number":"9469878610","city":"Tor","country":"Egypt","zip_code":null},
{"id":17,"name":"Lexi Durston","age":37,"office":"Schmitt LLC","joined_date":"06/12/2018","salary":29282,"email":"ldurstong@dion.ne.jp","phone_number":"4201903781","city":"Ipameri","country":"Brazil","zip_code":"75780-000"},
{"id":18,"name":"Baird Boorman","age":45,"office":"Hane, Murphy and Turner","joined_date":"24/12/2019","salary":48518,"email":"bboormanh@npr.org","phone_number":"9162097290","city":"Sacramento","country":"United States","zip_code":"94291"},
{"id":19,"name":"Myrtie Huygen","age":46,"office":"Raynor, Boehm and Heidenreich","joined_date":"07/10/2020","salary":27785,"email":"mhuygeni@cdc.gov","phone_number":"9728665123","city":"Concepcion","country":"Philippines","zip_code":"7213"},
{"id":20,"name":"Jude Wudeland","age":31,"office":"Macejkovic-Crist","joined_date":"05/01/2019","salary":23610,"email":"jwudelandj@blinklist.com","phone_number":"3446131006","city":"Gaohu","country":"Chennai","zip_code":null},
{"id":21,"name":"Carola Dudny","age":33,"office":"Hagenes-Kreiger","joined_date":"03/04/2020","salary":48796,"email":"cdudnyk@sitemeter.com","phone_number":"6023326585","city":"Troyes","country":"France","zip_code":"10081 CEDEX"},
{"id":22,"name":"Helen Stuttman","age":40,"office":"Hartmann and Sons","joined_date":"30/01/2020","salary":42520,"email":"hstuttmanl@uol.com.br","phone_number":"6012173903","city":"Cuauhtemoc","country":"Mexico","zip_code":"33102"},
{"id":23,"name":"Caterina Pinnock","age":40,"office":"Littel-Collins","joined_date":"12/07/2020","salary":44422,"email":"cpinnockm@goodreads.com","phone_number":"2386101349","city":"Ardazubre","country":"Portugal","zip_code":"3025-348"},
{"id":24,"name":"Teri Annies","age":28,"office":"Sawayn, Funk and Rutherford","joined_date":"03/08/2020","salary":27318,"email":"tanniesn@github.io","phone_number":"5287173101","city":"Carigara","country":"Philippines","zip_code":"6529"},
{"id":25,"name":"Leanora Loweth","age":41,"office":"Lemke, Collier and Koch","joined_date":"09/03/2020","salary":31069,"email":"llowetho@google.es","phone_number":"1061499579","city":"Akhmīm","country":"Egypt","zip_code":null},
{"id":26,"name":"Andromache Dallander","age":45,"office":"Bruen, Wiegand and Feest","joined_date":"23/05/2019","salary":41104,"email":"adallanderp@fc2.com","phone_number":"9466156080","city":"Trang","country":"Thailand","zip_code":"30110"},
{"id":27,"name":"Jessalin Cuseck","age":31,"office":"Koepp Group","joined_date":"09/11/2019","salary":44234,"email":"jcuseckq@si.edu","phone_number":"8661612391","city":"Newbiggin","country":"United Kingdom","zip_code":"NE46"},
{"id":28,"name":"Eydie Churms","age":48,"office":"Casper Group","joined_date":"02/12/2018","salary":23059,"email":"echurmsr@de.vu","phone_number":"1571954014","city":"Cordova","country":"Philippines","zip_code":"6017"},
{"id":29,"name":"Wini Ryrie","age":38,"office":"Considine-Walsh","joined_date":"26/06/2020","salary":29948,"email":"wryries@is.gd","phone_number":"4438523994","city":"Delhi","country":"India","zip_code":null},
{"id":30,"name":"Gabrielle Fontin","age":50,"office":"Smitham and Sons","joined_date":"08/02/2019","salary":42757,"email":"gfontint@altervista.org","phone_number":"9334128644","city":"Rū-ye Sang","country":"Afghanistan","zip_code":null},
{"id":31,"name":"Anitra Terlinden","age":50,"office":"McLaughlin-Sanford","joined_date":"27/10/2020","salary":42444,"email":"aterlindenu@reverbnation.com","phone_number":"8545547831","city":"La Gomera","country":"Guatemala","zip_code":"05007"},
{"id":32,"name":"Margo Adderley","age":28,"office":"Macejkovic Inc","joined_date":"05/12/2018","salary":41255,"email":"madderleyv@youtube.com","phone_number":"1202534144","city":"Belos Ares","country":"Portugal","zip_code":"4650-304"},
{"id":33,"name":"Trevor Presidey","age":43,"office":"Moore-Weissnat","joined_date":"19/11/2019","salary":46118,"email":"tpresideyw@sphinn.com","phone_number":"2882912820","city":"Boulogne-Billancourt","country":"France","zip_code":"92511 CEDEX"},
{"id":34,"name":"Joscelin Wims","age":46,"office":"Trantow, Bogisich and Wilkinson","joined_date":"29/03/2020","salary":33881,"email":"jwimsx@bandcamp.com","phone_number":"7369958227","city":"Uruaçu","country":"Brazil","zip_code":"76400-000"},
{"id":35,"name":"Kenna Innis","age":43,"office":"Koch Inc","joined_date":"06/12/2019","salary":35579,"email":"kinnisy@shutterfly.com","phone_number":"1607078089","city":"Pichilemu","country":"Chile","zip_code":null},
{"id":36,"name":"Jeremias Paiton","age":49,"office":"Hermiston Group","joined_date":"28/11/2018","salary":26850,"email":"jpaitonz@vk.com","phone_number":"4565425193","city":"Storvik","country":"Sweden","zip_code":"812 30"},
{"id":37,"name":"Fidelio Bickmore","age":46,"office":"Mueller, Glover and Toy","joined_date":"28/09/2020","salary":36019,"email":"fbickmore10@earthlink.net","phone_number":"8646614890","city":"Paprotnia","country":"Poland","zip_code":"08-107"},
{"id":38,"name":"Bernarr Skillern","age":34,"office":"Franecki-D'Amore","joined_date":"07/10/2019","salary":37273,"email":"bskillern11@vinaora.com","phone_number":"6904161222","city":"Chennai","country":"India","zip_code":null},
{"id":39,"name":"Sutton McGahy","age":49,"office":"Thiel-Collins","joined_date":"24/07/2020","salary":40411,"email":"smcgahy12@oracle.com","phone_number":"9167840753","city":"Ramadi","country":"Iraq","zip_code":null},
{"id":40,"name":"Lynna Wrathall","age":40,"office":"Hayes, Jerde and Towne","joined_date":"13/02/2020","salary":23820,"email":"lwrathall13@wp.com","phone_number":"7389537826","city":"Chennai","country":"India","zip_code":null},
{"id":41,"name":"Mimi Rubinovitsch","age":24,"office":"Renner-Casper","joined_date":"22/10/2020","salary":25887,"email":"mrubinovitsch14@sphinn.com","phone_number":"5827297649","city":"Stalingrad","country":"Russia","zip_code":"141196"},
{"id":42,"name":"Mohandas Parchment","age":49,"office":"Bogan and Sons","joined_date":"17/12/2018","salary":23178,"email":"mparchment15@pcworld.com","phone_number":"3958699507","city":"Kobylin","country":"Poland","zip_code":"63-740"},
{"id":43,"name":"Clywd Grebner","age":28,"office":"Keeling, Sanford and Schmeler","joined_date":"16/02/2020","salary":35091,"email":"cgrebner16@statcounter.com","phone_number":"5635456096","city":"Stalingrad","country":"Russia","zip_code":"652240"},
{"id":44,"name":"Taddeo Allaway","age":38,"office":"Wiegand, Lakin and Hauck","joined_date":"26/09/2019","salary":41035,"email":"tallaway17@reddit.com","phone_number":"3985573100","city":"Wieczfnia Kościelna","country":"Poland","zip_code":"06-513"},
{"id":45,"name":"Moses Landor","age":30,"office":"Thompson, Stroman and Jaskolski","joined_date":"21/05/2020","salary":26954,"email":"mlandor18@vinaora.com","phone_number":"6518107825","city":"Shemonaīkha","country":"Kazakhstan","zip_code":null},
{"id":46,"name":"Egor Charnick","age":35,"office":"Bergstrom LLC","joined_date":"23/12/2018","salary":45036,"email":"echarnick19@ca.gov","phone_number":"7597852234","city":"Chennai","country":"India","zip_code":null},
{"id":47,"name":"Ella Ascrofte","age":46,"office":"Boehm-Nader","joined_date":"11/09/2020","salary":32955,"email":"eascrofte1a@time.com","phone_number":"5578595673","city":"Crespo","country":"Argentina","zip_code":"3116"},
{"id":48,"name":"Brianne Paule","age":36,"office":"Monahan LLC","joined_date":"03/09/2020","salary":33176,"email":"bpaule1b@dropbox.com","phone_number":"3489322678","city":"Congkar","country":"Indonesia","zip_code":null},
{"id":49,"name":"Leontine Foxall","age":47,"office":"Haag, Muller and Sporer","joined_date":"05/03/2020","salary":23215,"email":"lfoxall1c@wix.com","phone_number":"4566230115","city":"Foxrock","country":"Ireland","zip_code":"D04"},
{"id":50,"name":"Barde Shortland","age":29,"office":"Willms, Kozey and Wolff","joined_date":"06/05/2019","salary":29824,"email":"bshortland1d@posterous.com","phone_number":"2622762157","city":"Wawer","country":"Poland","zip_code":"37-750"},
{"id":51,"name":"Griffin Tassell","age":23,"office":"Graham Inc","joined_date":"04/07/2020","salary":26759,"email":"gtassell1e@bloglovin.com","phone_number":"9966794712","city":"Kävlinge","country":"Sweden","zip_code":"244 24"},
{"id":52,"name":"Danika Wyon","age":33,"office":"Prohaska, Zieme and Wilderman","joined_date":"28/11/2019","salary":39168,"email":"dwyon1f@tripadvisor.com","phone_number":"7907320259","city":"Ares Tengah","country":"Indonesia","zip_code":null},
{"id":53,"name":"Sancho Haquard","age":32,"office":"Will, Hamill and Kautzer","joined_date":"04/05/2019","salary":26411,"email":"shaquard1g@youtube.com","phone_number":"1662846047","city":"Vuhlehirs’k","country":"Ukraine","zip_code":null},
{"id":54,"name":"Lissie Keach","age":45,"office":"Rowe Group","joined_date":"30/08/2020","salary":25760,"email":"lkeach1h@hp.com","phone_number":"5966436662","city":"Dashkawka","country":"Belarus","zip_code":null},
{"id":55,"name":"Jacky Amort","age":44,"office":"Howell-Labadie","joined_date":"23/08/2019","salary":41415,"email":"jamort1i@walmart.com","phone_number":"6107477026","city":"Oxbow","country":"Canada","zip_code":"T9M"},
{"id":56,"name":"Danya Boxer","age":46,"office":"Baumbach, Stark and Hilll","joined_date":"29/01/2020","salary":46305,"email":"dboxer1j@about.com","phone_number":"6515390186","city":"Chak Two Hundred Forty-Nine TDA","country":"Pakistan","zip_code":"19030"},
{"id":57,"name":"Sue Krale","age":43,"office":"Treutel, Swaniawski and Schuppe","joined_date":"10/03/2020","salary":39220,"email":"skrale1k@bloglovin.com","phone_number":"5959906721","city":"Marmashen","country":"Armenia","zip_code":null},
{"id":58,"name":"Luise Fell","age":22,"office":"Cole, Bogisich and Graham","joined_date":"04/02/2020","salary":48829,"email":"lfell1l@icio.us","phone_number":"5288280801","city":"Waturoka","country":"Indonesia","zip_code":null},
{"id":59,"name":"Adrienne Cherm","age":28,"office":"Baumbach-Kertzmann","joined_date":"11/02/2019","salary":36455,"email":"acherm1m@google.de","phone_number":"6693406727","city":"Eskifjörður","country":"Iceland","zip_code":"735"},
{"id":60,"name":"Jedidiah Mosdall","age":39,"office":"Kunde, Macejkovic and Konopelski","joined_date":"04/09/2019","salary":43443,"email":"jmosdall1n@apache.org","phone_number":"2141856987","city":"Dallas","country":"United States","zip_code":"75310"},
{"id":61,"name":"Zandra Dey","age":28,"office":"Lowe and Sons","joined_date":"30/05/2020","salary":30624,"email":"zdey1o@wikipedia.org","phone_number":"4841057625","city":"Chennai","country":"India","zip_code":null},
{"id":62,"name":"Naoma Norsister","age":28,"office":"Lockman, Runolfsson and Predovic","joined_date":"12/02/2019","salary":38950,"email":"nnorsister1p@delicious.com","phone_number":"5085933744","city":"Ollachea","country":"Peru","zip_code":null},
{"id":63,"name":"Natty Zecchinelli","age":46,"office":"Wehner-Reichel","joined_date":"17/10/2020","salary":36868,"email":"nzecchinelli1q@quantcast.com","phone_number":"9702436711","city":"Chennai","country":"India","zip_code":null},
{"id":64,"name":"Talya Cannavan","age":28,"office":"Wyman, Hand and Zboncak","joined_date":"27/02/2020","salary":38870,"email":"tcannavan1r@tiny.cc","phone_number":"1144227906","city":"Đắk Song","country":"Vietnam","zip_code":null},
{"id":65,"name":"Arne Gethins","age":24,"office":"Dickens-Tromp","joined_date":"08/09/2019","salary":23161,"email":"agethins1s@ed.gov","phone_number":"3096882241","city":"Chennai","country":"India","zip_code":null},
{"id":66,"name":"Juli Pashen","age":22,"office":"Hansen Inc","joined_date":"05/02/2020","salary":36145,"email":"jpashen1t@ca.gov","phone_number":"2609967462","city":"Dallas","country":"United States","zip_code":"46896"},
{"id":67,"name":"Fallon Nangle","age":38,"office":"Will LLC","joined_date":"20/06/2020","salary":23444,"email":"fnangle1u@jalbum.net","phone_number":"5228237286","city":"Melun","country":"France","zip_code":"77050 CEDEX"},
{"id":68,"name":"Tracie Truin","age":43,"office":"Gerlach LLC","joined_date":"27/12/2018","salary":46891,"email":"ttruin1v@lulu.com","phone_number":"4358714132","city":"Mumbai","country":"India","zip_code":null},
{"id":69,"name":"Emilie Taft","age":26,"office":"Witting, Mayer and Rau","joined_date":"24/11/2018","salary":48343,"email":"etaft1w@businessweek.com","phone_number":"1646155589","city":"Mumbai","country":"India","zip_code":null},
{"id":70,"name":"Selma Enderlein","age":44,"office":"Anderson, Gottlieb and Batz","joined_date":"16/01/2019","salary":41979,"email":"senderlein1x@webs.com","phone_number":"9113995475","city":"Monguno","country":"Nigeria","zip_code":null},
{"id":71,"name":"Jeniffer Crowson","age":34,"office":"Fay, Pacocha and Macejkovic","joined_date":"23/09/2020","salary":39098,"email":"jcrowson1y@uiuc.edu","phone_number":"9348789734","city":"Sampués","country":"Colombia","zip_code":"705079"},
{"id":72,"name":"Peter Thain","age":27,"office":"Connelly and Sons","joined_date":"26/11/2018","salary":36977,"email":"pthain1z@dyndns.org","phone_number":"3386269052","city":"Mumbai","country":"India","zip_code":null},
{"id":73,"name":"Russell Cartman","age":36,"office":"Feeney, Conn and Hoeger","joined_date":"02/04/2019","salary":30880,"email":"rcartman20@toplist.cz","phone_number":"9763943200","city":"Dinan","country":"France","zip_code":"22104 CEDEX"},
{"id":74,"name":"Carry Ofield","age":23,"office":"Bernhard and Sons","joined_date":"14/07/2020","salary":47088,"email":"cofield21@pinterest.com","phone_number":"7914104190","city":"Moscow","country":"Russia","zip_code":"197730"},
{"id":75,"name":"Marion Pinkard","age":49,"office":"Kihn, Kuhic and Gusikowski","joined_date":"24/01/2019","salary":24749,"email":"mpinkard22@prweb.com","phone_number":"2725545583","city":"Sadananya","country":"Indonesia","zip_code":null},
{"id":76,"name":"Dannye Humbee","age":45,"office":"Koch and Sons","joined_date":"17/12/2019","salary":46198,"email":"dhumbee23@skype.com","phone_number":"6715826390","city":"Moscow","country":"Russia","zip_code":"412787"},
{"id":77,"name":"Augustine Valde","age":28,"office":"Lind-Gleichner","joined_date":"28/07/2020","salary":38120,"email":"avalde24@google.co.jp","phone_number":"6364075123","city":"Oslo","country":"Norway","zip_code":"0620"},
{"id":78,"name":"Stirling Jaskowicz","age":34,"office":"Runolfsdottir-Ruecker","joined_date":"21/03/2020","salary":32552,"email":"sjaskowicz25@canalblog.com","phone_number":"1596413771","city":"Mumbai","country":"India","zip_code":"532663"},
{"id":79,"name":"Faustina Dearn","age":37,"office":"Nolan, Kub and Farrell","joined_date":"27/10/2020","salary":35793,"email":"fdearn26@google.de","phone_number":"2924639737","city":"New Leyte","country":"Philippines","zip_code":"8102"},
{"id":80,"name":"Bernardine Wheelton","age":29,"office":"Lesch-Stark","joined_date":"29/03/2020","salary":48602,"email":"bwheelton27@engadget.com","phone_number":"7383945630","city":"Mumbai","country":"India","zip_code":null},
{"id":81,"name":"Cal Lodeke","age":44,"office":"Sporer, Homenick and Bauch","joined_date":"28/05/2020","salary":38028,"email":"clodeke28@storify.com","phone_number":"9118640884","city":"Fontvieille","country":"Monaco","zip_code":"98000"},
{"id":82,"name":"Milzie Kingsworth","age":27,"office":"Hand-Schamberger","joined_date":"05/08/2019","salary":31372,"email":"mkingsworth29@wikimedia.org","phone_number":"1274703844","city":"Osorno","country":"Chile","zip_code":null},
{"id":83,"name":"Candy Yurasov","age":38,"office":"Kessler, Nolan and Schmitt","joined_date":"10/01/2020","salary":23595,"email":"cyurasov2a@timesonline.co.uk","phone_number":"7059911714","city":"Kolkata","country":"India","zip_code":null},
{"id":84,"name":"Aloise Dickin","age":31,"office":"Upton, Johnson and Robel","joined_date":"13/02/2019","salary":44884,"email":"adickin2b@blog.com","phone_number":"2944618655","city":"Ambatolampy","country":"Madagascar","zip_code":null},
{"id":85,"name":"Lars Gansbuhler","age":46,"office":"Runolfsson and Sons","joined_date":"29/01/2020","salary":38734,"email":"lgansbuhler2c@jugem.jp","phone_number":"4038187527","city":"Marbel","country":"Philippines","zip_code":"9406"},
{"id":86,"name":"Erskine Tumasian","age":41,"office":"Sporer, Kuhn and Farrell","joined_date":"02/03/2019","salary":29989,"email":"etumasian2d@mail.ru","phone_number":"8997313461","city":"Moscow","country":"Russia","zip_code":"669469"},
{"id":87,"name":"Mitzi Sola","age":27,"office":"Bergstrom and Sons","joined_date":"31/03/2020","salary":46837,"email":"msola2e@dot.gov","phone_number":"2332556531","city":"Kavadarci","country":"Macedonia","zip_code":"1430"},
{"id":88,"name":"Imelda Lapwood","age":38,"office":"Steuber Inc","joined_date":"25/10/2019","salary":23205,"email":"ilapwood2f@newsvine.com","phone_number":"5456034353","city":"Takaishi","country":"Japan","zip_code":"215-0003"},
{"id":89,"name":"Charmine Chastney","age":38,"office":"Friesen and Sons","joined_date":"13/11/2019","salary":41834,"email":"cchastney2g@nasa.gov","phone_number":"4461963130","city":"Luntas","country":"Indonesia","zip_code":null},
{"id":90,"name":"Fernande Joisce","age":38,"office":"Carroll-Gerlach","joined_date":"22/07/2019","salary":43896,"email":"fjoisce2h@ask.com","phone_number":"7219699837","city":"Posse","country":"Brazil","zip_code":"73900-000"},
{"id":91,"name":"Krystyna Penwright","age":27,"office":"Dibbert-McLaughlin","joined_date":"28/12/2019","salary":26731,"email":"kpenwright2i@histats.com","phone_number":"5308872718","city":"Būrabay","country":"Kazakhstan","zip_code":null},
{"id":92,"name":"Lemuel Postlewhite","age":41,"office":"Cartwright-Feeney","joined_date":"21/09/2019","salary":32681,"email":"lpostlewhite2j@yandex.ru","phone_number":"9138704135","city":"Kolkata","country":"India","zip_code":null},
{"id":93,"name":"Lona Brankley","age":24,"office":"Conroy LLC","joined_date":"19/05/2020","salary":30748,"email":"lbrankley2k@bing.com","phone_number":"7698788002","city":"Cimo de Vila","country":"Portugal","zip_code":"4615-092"},
{"id":94,"name":"Claire Dizlie","age":29,"office":"Fisher Group","joined_date":"29/01/2019","salary":35569,"email":"cdizlie2l@barnesandnoble.com","phone_number":"7353116928","city":"Sousse","country":"Tunisia","zip_code":null},
{"id":95,"name":"Aili Tremmil","age":23,"office":"Kunde LLC","joined_date":"17/01/2019","salary":23287,"email":"atremmil2m@va.gov","phone_number":"1272800293","city":"Santi Suk","country":"Thailand","zip_code":"61150"},
{"id":96,"name":"Olympe O'Lunny","age":26,"office":"Carter Inc","joined_date":"17/11/2019","salary":41948,"email":"oolunny2n@virginia.edu","phone_number":"9758335591","city":"Sorochuco","country":"Peru","zip_code":null},
{"id":97,"name":"Celie Cornell","age":50,"office":"Shanahan-Marquardt","joined_date":"28/10/2020","salary":33445,"email":"ccornell2o@yahoo.com","phone_number":"5128872439","city":"Kolkata","country":"India","zip_code":null},
{"id":98,"name":"Kristien Haulkham","age":39,"office":"Schuppe-Wisozk","joined_date":"23/06/2019","salary":46976,"email":"khaulkham2p@bizjournals.com","phone_number":"5988838380","city":"Novohrad-Volyns’kyy","country":"Ukraine","zip_code":null},
{"id":99,"name":"Korie Prydie","age":40,"office":"Ratke-Windler","joined_date":"27/07/2019","salary":28912,"email":"kprydie2q@businessweek.com","phone_number":"7579432080","city":"Krajan Kulon","country":"Indonesia","zip_code":null},
{"id":100,"name":"Iggie Pidgeley","age":27,"office":"Boyer-Senger","joined_date":"17/11/2020","salary":47820,"email":"ipidgeley2r@independent.co.uk","phone_number":"2217712657","city":"Kolkata","country":"India","zip_code":null}];

var userData = {
  users: testUserData
}

ko.applyBindings(new UserPaginationModel(userData));

